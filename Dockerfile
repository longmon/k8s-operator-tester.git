FROM golang:1.16.8 as builder

WORKDIR /workspace

COPY . .

ENV GOPROXY=https://goproxy.io,direct 

RUN CGO_ENABLED=0 go build -a -ldflags '-extldflags "-static"' -o k8sOperatorTest *.go

FROM alpine:3.14

WORKDIR /

COPY --from=builder /workspace/k8sOperatorTest /k8sOperatorTest

ENTRYPOINT ["/k8sOperatorTest"]
