APP=k8sOperatorTest

.PHONY: dev
dev:
	go run *.go

.PHONY: build
build:
	nerdctl build -t registry.cn-shenzhen.aliyuncs.com/longmon/k8s-operator-test:v1 .
