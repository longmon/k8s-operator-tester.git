package errs

import "fmt"

func New(data interface{}) error {
	return fmt.Errorf("%v", data)
}
