package logs

import (
	"fmt"
	"log"
	"runtime"
)

func init() {
	log.SetFlags(log.LstdFlags|log.Lshortfile)
}

func Debug(data ...interface{}) {
	_, file,line, ok := runtime.Caller(1)
	if !ok {
		file = "???"
		line = 0
	}
	log.SetPrefix(fmt.Sprintf("%s(%d): ", file, line))
	log.Println(data)
}
