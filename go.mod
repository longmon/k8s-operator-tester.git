module k8s-operator-test

go 1.16

require (
	github.com/gin-gonic/gin v1.7.4
	github.com/spf13/cobra v1.2.1
	gopkg.in/ffmt.v1 v1.5.6
	k8s.io/apimachinery v0.22.2
	k8s.io/client-go v0.22.2
)
